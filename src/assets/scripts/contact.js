/* requires:
jquery.validationEngine.js
jquery.validationEngine-es.js
*/

$(()=>{
	$("contact-form").validationEngine('attach', {
		promptPosition : "topLeft",
		autoHidePrompt: true,
		autoHideDelay: 3000,
		binded: false,
		scroll: false,
		validateNonVisibleFields: true
	});

	$("#form-contac").submit(function(e) {
		var valid = $(this).validationEngine('validate');
		if (!valid){
			return false;
		}else{
			return true;
		};
	});	
})