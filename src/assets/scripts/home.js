/* requires:
	lightgallery.js
	jquery.bxslider.min.js
	lg-fullscreen.js
	lg-zoom.js
	lg-video.js
	jquery.waypoints.js
*/
$(()=>{
	$('.b1-contenedor').bxSlider({
		mode: 'fade',
		controls:false,
		pager:true,
		adaptiveHeight:true,
		auto:true,
		onSliderLoad: function(currentIndex) {
		$('.b1-contenedor').children().eq(currentIndex).addClass('active');
		},
		onSlideBefore: function($slideElement){
		$('.b1-contenedor').children().removeClass('active');
		setTimeout( function(){
		$slideElement.addClass('active');
		},500);
		}
	});
	// fancy video
	$('.videoGallery').lightGallery({
		selector: 'a'
	});
	//waypoint
	var b4 = $('.b4');
		b4.waypoint(function(direction) {
		if (direction === 'down') {
		$('.b4-content-box').addClass('b4-anima');
		}
		}, {
	offset:'65%'
	});

})

