/* requires:
	slick.min.js
*/
$(()=>{
	$('.b10-contenedor-slider').slick({
		dots: true,
		infinite: true,
		arrows:false,
		speed: 300,
		fade: true,
		slidesToShow: 1,
		adaptiveHeight: true
	});

})

